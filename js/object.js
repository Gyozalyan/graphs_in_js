const name = 'Misha';
const age = 21;
const color = 'green';

function getName() {
    return this.name;
}

function getPropName() {
    return 'someDynamicValue';
}

const user = {
    name,
    age,
    getName,
    getAge() {
        return this.age;
    },
    [1+1]:2,
    [getPropName()]: 'I say something',
    [color]:'black',
};

console.log(user);
console.log(user.getName());
console.log(user.getAge());
