
// Rest  operator 
function getSum() {
   
    // FOR SUM 
 // let sum = 0;
 // for (let i = 0; i < arguments.length; i++ ){
 //     sum += arguments[i]
 // }
 // return sum;
    
 let newArray = Array.prototype.slice.call(arguments,0);
 
    return newArray.reduce(function(res, item){
        return res + item;
    });

}

console.log(getSum(13,22,34));


function newGetArgs(a, b, ...args) {
    console.log(a, b);
    return args.reduce(function(res, item){
        return res + item;
    });
}


console.log(newGetArgs(13,22,34, 345, 434));

// SPREAD

const firstUser = {
    name: 'Misha',
    age: 21,
    width: 32
};

const data = {
    color: 'yellow',
    goal: [],
    width: 20
}; 

const result = {
    name: 'Misha',
    age: 21,
    color: 'yellow',
    width: 40
}

const resultObject = Object.assign({} ,data, result, {newValue: 'NewValue'})

console.log(resultObject);

// NEW SPREAD

const newResultObject = {
    ...firstUser,
    ...data,
    newValue: 'newValue'
}
console.log(newResultObject);


const firstArray = [1,34,56,67,76,43];
const secondArray = [34,556,74,2,3,6];
// Masivneri miavorum concatov
const resultArray = firstArray.concat(secondArray);
console.log(resultArray);
// Masivneri miavorum concatov

const newResultArray = [
    ...firstArray.slice(1,4),
    ...secondArray,
    'asas'
];
console.log(newResultArray);




//////////// ForOf

function getSum() {
    for (let value of arguments) {
        console.log(value)
    }
}
console.log(getSum (4, 55, 6));


const id = Symbol();

const user = {
    name: 'Misha',
    age: 21,
    [id]: 'fsdffs scc'
};


console.log(user);

for (let key in user ) {
    console.log(user[key])
};
 
user.id = 'NEWID';
var first = Symbol.for('first');
var second = Symbol.for('first');
console.log(user[id],first === second);
